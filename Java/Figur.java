import java.awt.Color;

/* Klasse zum Zusammenfassen aller Figuren auf dem Spielfeld */
abstract class Figur
{
    protected Punkt position;       // Position
    
    protected String bezeichnung;   // Bezeichnung
    
    protected Color farbe;          // Farbe
    
    public Figur()
    {
          
    }
    
    // abstrakte Methoden
    abstract int minX();
    abstract int minY();
    abstract int maxX();
    abstract int maxY();
    
    // get Position
    public Punkt getPosition()
    {
        return position;
    }
    
    // set Position
    public void setPosition(Punkt position)
    {
        this.position = position;
    }
    
    // get Bezeichnung
    public String getBezeichnung()
    {
        return bezeichnung;
    }
    
    //set Sezeichnung
    public void setBezeichnung(String bezeichnung)
    {
        this.bezeichnung = bezeichnung;
    }
    
    //get Farbe
    public Color getFarbe()
    {
        return farbe;
    }
    
    //set Farbe
    public void setFarbe(Color farbe)
    {
        this.farbe = farbe;
        if (farbe == java.awt.Color.white)
        {
            System.out.println("Weiß ist keine gültige Farbe!");
        }
            
    }
    
    // bewege um Parameter
    public void bewegeUm(int dx, int dy)
    {
        position.bewegeUm(dx, dy);
    }
    
    // bewege um Verschiebevektor
    public void bewegeUm(Punkt verschiebevektor)
    {
        position.bewegeUm(verschiebevektor.getX(),verschiebevektor.getY());
    }
    
    
}
