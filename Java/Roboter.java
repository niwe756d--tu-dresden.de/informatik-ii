import java.util.*;
import java.awt.Color;

/* Klasse zur Bestimmung des Roboters */
public class Roboter extends Kreis
{
    private enum Stichwort{NAME, ALTER, HERSTELLER, GESCHLECHT};        // enumeration der Stichwoerter
    
    public Roboter()
    {
        farbe = java.awt.Color.black;       // Farbe des Roboters
        diameter = 15;                      // Durchmesser 
        position = new Punkt(0,0);          // Ausgangsposition
        bezeichnung = "Paradise";           // Name
    }
    
    // erlaubt es dem User Fragen an den Roboter zu stellen und diese beantwortet zu bekommen
    public void spracherkennung()
    {
        boolean a = true;       // Hilfsvariable fuer die while loop
            
        while(a)
        { 
            System.out.println("Bitte stellen Sie mir eine Frage");
            Scanner sc = new Scanner(System.in);    // Initialisierung Scanner
            String eingabe = sc.nextLine();         // Eingabe ist, was der User ueber Konsole eingibt
            eingabe = eingabe.toUpperCase();        // Eingabe wird alles zu Großbuchstaben
            
            // ueberprueft jedes Stichwort
            for (Stichwort s : Stichwort.values())
            {
                // ueberprueft, ob in der Eingabe das jeweilige Stichwort enthalten ist
                if (eingabe.contains(s.toString()))
                {
                    String key = s.toString();  
                    // je nach gefundenen Stichwort wird die passende Antwort wiedergegeben
                    switch(key) 
                    {
                        case "NAME":
                            System.out.println("Mein Name ist Paradise :D");
                            break;
                                
                        case "ALTER":
                            System.out.println("Ich bin ein Jahr alt.");
                            break;
                                
                        case "HERSTELLER":
                            System.out.println("Ich wurde von Alex und Nick erschaffen.");
                            break;
                                
                        case "GESCHLECHT":
                            System.out.println("Ich weiß nicht, was ich bin - Alles was zählt ist, dass ich glücklich bin.");
                            break;
                                
                        default:
                            break;         
                    }                 
                }
            }          
            
            // Abbruch, falls der User "ende" eingibt
            if (eingabe.equals("ENDE"))
            {
                break;
            }
        }
    }
    
    // ueberprueft, ob der Roboter sich an einer Wand befindet
    public boolean anWandX(int WandX)
    {
        boolean anWandX;
        anWandX = false;     // standard auf 0 gesetzt
        
        // falls max x mit einer Wand uebereinstimmt, befindet er sich an der x Wand
        if (maxX() == WandX - 1)
        {
            anWandX = true;
        }
        
        return anWandX;
    }
    
        // ueberprueft, ob der Roboter sich an einer Wand befindet
    public boolean anWandY(int WandY)
    {
        boolean anWandY;
        anWandY = false;     // standard auf 0 gesetzt
        
        // falls max y mit einer Wand uebereinstimmt, befindet er sich an der y Wand
        if (maxY() == WandY - 1)
        {
            anWandY = true;
        }
        
        return anWandY;
    }
    
    // ueberprueft, ob der Roboter sich zwischen den x - Werten einer Figur befindet
    public boolean zwischenX(Figur figur)
    {
        boolean zwischenX;
        zwischenX = false;
        
        // gibt bereits ein Pixel vor "Betreten" der x - Werte Bescheid, bzw. erst einen danach, damit di Kanten nicht uebereinander liegen
        if (maxX() >= figur.minX()-1  && minX() <= figur.maxX()+1 && (maxY() == figur.minY()-1))
        {
            zwischenX = true;
        }
        return zwischenX;
    }
    
    // ueberprueft, ob der Roboter sich zwischen den y - Werten einer Figur befindet
    public boolean zwischenY(Figur figur)
    {
        boolean zwischenY;
        zwischenY = false;
        
        // gibt bereits ein Pixel vor "Betreten" der y - Werte Bescheid, bzw. erst einen danach, damit di Kanten nicht uebereinander liegen
        if (maxY() >= figur.minY()-1 && minY() <= figur.maxY()+1 && (maxX() == figur.minX()-1))
        {
            zwischenY = true;
        }
        return zwischenY;
    }
    
    public boolean ueberlapptRoboter(Rechteck r)
    {
        return !((maxX() + 1 < r.minX() | r.maxX() + 1 < minX()) & (r.maxY() +1 < minY() | maxY() + 1 < r.minY()));
    }
}