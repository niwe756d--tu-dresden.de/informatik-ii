import java.awt.Color;

/* Klasse um Rechtecke zu repraesentieren */
public class Rechteck extends Figur
{
    private int breite;     // Breite des Rechtecks 
    private int laenge;      // Laenge des Rechtecks
    
    public Rechteck()
    {
    }

    public Rechteck(Punkt position, int breite, int laenge, String bezeichnung, Color farbe)
    {
        this.position = position;
        this.breite = breite;
        this.laenge = laenge;
        this.bezeichnung = bezeichnung;
        this.farbe = farbe;
    }
    
    // get Breite
    public int getBreite()
    {
        return breite;
    }
    
    // set Breite
    public void setBreite(int breite)
    {
        this.breite = breite;
    }
    
    // get Laenge
    public int getLaenge()
    {
        return laenge;
    }
    
    // set Laenge
    public void setLaenge(int länge)
    {
        this.laenge = laenge;
    }
    
    // Ausgabe Attribute
    public void ausgabeAttribute()
    {
        System.out.println("(" + position + ";" + breite + ";" + laenge + 
        ";" + bezeichnung + ";" + farbe + ")");
    }
    
    // ueberprueft, ob sich zwei Rechtecke ueberlappen
    public boolean ueberlappt(Rechteck r) 
    {
        return !(position.getX() + getBreite() + 1 < r.position.getX() | r.position.getX() + r.getBreite() + 1 < position.getX() & r.position.getY() + r.getLaenge() +
        1 < position.getY() | position.getY() + getLaenge() + 1 < r.position.getY());
    }
    
    // minimaler x - Wert
    public int minX()
    {
        return position.getX();
    }
    
    // minimaler y - Wert
    public int minY()
    {
        return position.getY();
    }
    
    // maximaler x - Wert
    public int maxX()
    {
        return (position.getX() + getBreite());
    }
    
    // maximaler y - Wert
    public int maxY()
    {
        return (position.getY() + getLaenge());
    }
}

