import javax.swing.*;
import java.util.*;
import java.awt.*;

/*Klasse Leinwand ist verantwortlich dafuer, die Leinwand mit hindernissen und Roboter zu erstellen */
public class Leinwand 
{
    private JFrame fenster; // erstellt Fenster
    private Zeichenflaeche zeichenflaeche;  // Initialisierung der zeichenflaeche
    private int laenge; //Attribut laenge der Leinwand
    private int breite; // Attribut breite der Leinwand
    private Roboter r; //Attribut Roboter
    private static Leinwand leinwand = new Leinwand(); //Initialisierung der leinwand
    
    //Singleton Muster - static Methode greift auf die private Methode Leinwand zu, somit wird nur eine Leinwand erstellt 
    public static Leinwand gibLeinwand()
    {
        return leinwand;
    }
    
    //Konstruktor Leinwand
    private Leinwand()
    {
    }
    
    //notwendigen Parameter einstellen
    public void setParameter(int breite, int laenge, Roboter r)
    {
        this.breite = breite;
        this.laenge = laenge;
        this.r = r;
        zeichenflaeche = new Zeichenflaeche(r);     // Erstellen einer neuen zeichenflaeche
        fenster = new JFrame("Titel");              // Erstellt neues Fenster
        fenster.setSize(breite,laenge);             // Fenster wird auf Groeße des Spielfeldes gesetzt
        fenster.add(zeichenflaeche);                // Zeichenflaeche ins Fenster implementiert
        fenster.setVisible(true);                   // Fenster sichtbar machen
    }
    
    // wartet
    public void warten(int milisekunden)
    {
        try
        {
            Thread.sleep(milisekunden);
        }
        catch(Exception e)
        {
        }
    }
    
    // zeichnet Hindernisse auf die Zeichenflaeche
    public void zeichnen(ArrayList<Figur> hindernisse)
    {
        zeichenflaeche.repaintFiguren(hindernisse);
    }
}