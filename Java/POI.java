
/* Klasse POI ist dafuer zustaendig, die graphischen Eigenschaften der POI festzulegen */
public class POI extends Kreis
{
    public POI(Punkt position)
    {
        this.position = position;
        this.farbe = java.awt.Color.red;
        this.diameter = 5;
    }
}
