import java.util.*;
import javax.swing.*;
import java.awt.*;

/*Subklasse von JPanel - malt die Figuren*/
public class Zeichenflaeche extends JPanel
    {
        private ArrayList<Figur> hindernisse; // Initialisierung Array von Hindernissen
        private Roboter r;      // Initialisierung eines Roboters
        
        public Zeichenflaeche(Roboter r)
        {
            this.r = r;     // Erstellen eines Roboters
            this.hindernisse = new ArrayList<>();
            this.setVisible(true);
        }
        
        // Zeichnet alle Hindernisse
        public void paintComponent(Graphics g)
        {   
            Graphics2D g2 = (Graphics2D) g;
            
            g2.setColor(Color.WHITE);
            g2.fillRect(0,0, Spielfeld.breite, Spielfeld.laenge);
            for (int a = 0; a < hindernisse.size(); a++)
            {   
                if (hindernisse.get(a).getClass()==Rechteck.class)
                {
                    // zeichnet Umrisse des Rechtecks
                    g2.drawRect(hindernisse.get(a).getPosition().getX(),hindernisse.get(a).getPosition().getY(),
                                hindernisse.get(a).maxX() - hindernisse.get(a).minX(),
                                hindernisse.get(a).maxY() - hindernisse.get(a).minY());
                    // setzt die Farbe des Rechtecks
                    g2.setColor(hindernisse.get(a).getFarbe());
                    // fuellt das Rechteck mit der gegebenen Farbe
                    g2.fillRect(hindernisse.get(a).getPosition().getX(),hindernisse.get(a).getPosition().getY(),
                                hindernisse.get(a).maxX() - hindernisse.get(a).minX(),
                                hindernisse.get(a).maxY() - hindernisse.get(a).minY());
                }
                else
                {
                    // zeichnet Umrisse des Kreises
                    g2.drawOval(hindernisse.get(a).getPosition().getX(), hindernisse.get(a).getPosition().getY(), 
                                hindernisse.get(a).maxX() - hindernisse.get(a).minX(),
                                hindernisse.get(a).maxY() - hindernisse.get(a).minY());
                    // setzt die Farbe des Kreises
                    g2.setColor(hindernisse.get(a).getFarbe());
                    // fuellt den Kreis mit der gegebenen Farbe
                    g2.fillOval(hindernisse.get(a).getPosition().getX(), hindernisse.get(a).getPosition().getY(), 
                                hindernisse.get(a).maxX() - hindernisse.get(a).minX(),
                                hindernisse.get(a).maxY() - hindernisse.get(a).minY());
                }
            }
            // zeichnet den Roboter
            g2.drawOval(r.getPosition().getX(),r.getPosition().getY(),r.getDiameter(),r.getDiameter());     // Umriss (Kreis)
            g2.setColor(r.getFarbe());       // Farbe
            g2.fillOval(r.getPosition().getX(),r.getPosition().getY(),r.getDiameter(),r.getDiameter());      // mit Farbe ausfuellen
        }
        
        // malt die Figuren
        public void repaintFiguren(ArrayList<Figur> figuren)
        {
            hindernisse = figuren;
            repaint();
        }
    }