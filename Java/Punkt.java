import java.lang.Math; 

/* Klasse Punkt ist die Grundlage fuer fast alle anderen Funktionen des Programms (konzentriert sich auf Punkte)*/
public class Punkt
{
    private int x;      // x - Koordinate
    private int y;      // y - Koordinate
    
    public Punkt ()
    {
    }

    public Punkt (int x, int y)
    {
        this.x = x;
        this.y = y;
    }
   
    //x ausgeben
    public int getX()
    {
        return x;
    }
    
    //y ausgeben
    public int getY()
    {
        return y;
    }
    
    //x festlegen
    public void setX(int x)
    {
        this.x = x;
    }

    // y festlegen
    public void setY(int y)
    {
        this.y = y;
    }
    
    // x,y auf Konsole ausgeben
    public void ausgabeAttribute()
    {
        System.out.println("(" + x + ";" + y + ")");
    }
    
    // bewege um dx, dy Pixel
    public void bewegeUm(int dx, int dy)
    {
        x += dx;
        y += dy;
    }
    
    // Abstand zwischen zwei Punkten
    public double gibAbstand(int x2, int y2)
    {
        int x1 = getX();
        int y1 = getY();
        return Math.sqrt(Math.pow(x2-x1, 2)+Math.pow(y2-y1, 2));
    }
}
