/* Klasse zum erstellen der Figur des Roboters
Die Position des Kreises ist angegeben durch die obere linke Ecke des umschließenden Quadrates */
public class Kreis extends Figur
{
    protected int diameter;     // Durchmesser
    
    public Kreis()
    {
        
    }
    
    // set Druchmesser
    public void setDiameter(int Diameter)
    {
        this.diameter = diameter;
    }
    
    // get Druchmesser
    public int getDiameter()
    {
        return diameter;
    }
    
    // minimaler X - Wert
    public int minX()
    {
        return position.getX();
    }
    
    // minimaler Y - Wert
    public int minY()
    {
        return position.getY();
    }
    
    // maximaler X - Wert
    public int maxX()
    {
        return (position.getX() + getDiameter());
    }
    
    // maximaler Y - Wert
    public int maxY()
    {
        return (position.getY() + getDiameter());
    }
}
