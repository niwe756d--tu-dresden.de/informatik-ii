import java.util.*;
import java.awt.Color;
import java.lang.Math;

/*
 Die Klasse Spielfeld ist die zentrale Klasse dieses Programms. Ueber sie wird nicht nur das Spielfeld erzeugt, 
 sondern auch die main Methode ausgefuehrt, welche es ermoeglicht, auf alle anderen Hauptfeatures zuzugreifen.
 */
public class Spielfeld
{
    public static int breite;
    public static int laenge;
    private Punkt[] punkte_array;
    private static Random zufallsgenerator = new Random();
    private Roboter r;
    private Leinwand leinwand;

    public Spielfeld()
    {
        r = new Roboter();
        // Festlegen der Spielfeldgröße
        breite = 1000;                                                                   
        laenge = 1000;
        leinwand = Leinwand.gibLeinwand(); //gibt Leinwand, falls sie schon existiert, erstellt sie falls sie noch nicht existiert
        leinwand.setParameter(breite,laenge,r); //setzt parameter der Leinwand
    }
    
    // Methode gibt Breite des Spielfeldes wieder
    public int getBreite()  
    {
        return breite;
    }
    
    // Methode gibt Laenge des Spielfeldes wieder
    public int getLaenge()
    {
        return laenge;
    }
    
    // Methode ermöglicht es über Eingabe durch Konsole ein Array aus Punkten zu erstellen
    public Punkt[] punkteEingeben()
    {
        System.out.println("Wie viele Punkte wollen Sie erstellen?");
        Scanner sc = new Scanner(System.in); // scanner starten
        
        // try catched Eingabefehler bei den Angaben der Punkte heraus
        // falls ein Error entsteht wird der User gebeten nur ganze Koordinaten anzugeben
        try
        {
            int anzahl = sc.nextInt(); // Anzahl der zu erstellenden Punkte

            punkte_array = new Punkt[anzahl]; // Erstellen des Punkte Arrays

            punkte_array[0] = new Punkt(); // erster Punkt ist immer (0,0) = Startpunkt

            anzahl --;  // Anzahl Punkte um eins verringert, weil erster Punkt immer (0,0)

            while (anzahl >= 1)
            {
                // Abfragen der gewuenschten Werte für die jeweiligen Koordinaten
                System.out.println("x - Koordinate für Punkt " + anzahl);

                int x = sc.nextInt();   // x Koordinate 

                System.out.println("y - Koordinate für Punkt " + anzahl);

                int y = sc.nextInt();   // y Koordinate
                
                // Ueberpruefen, ob Punkt im Spielfeld liegt
                if (x>breite || y>laenge)
                {
                    System.out.println("Der Punkt liegt außerhalb des Spielfeldes");
                    anzahl += 1; // counter wird hochgesetzt, damit ein neuer, gueltiger Punkt an dem Index erstellt wird
                }

                else
                {
                    punkte_array[anzahl] = new Punkt(x,y); // Punkt wird dem Array hinzugefuegt, 
                }
                anzahl -= 1; // Anzahl verringern
            }
        }    
        catch (InputMismatchException e)
        {
            System.out.println("Bitte geben Sie nur ganze Zahlen an.");
        }
        finally
        {
            sc.close(); // Scanner schließen
        }

        return punkte_array;    
    }
    
    // sortiert ein array von Punkten, sodass der kuerzeste Weg genommen wird
    public Punkt[] poiSortieren(Punkt[] poi)
    {
        int x;      //x-koordinate des Bezugspunktes
        int y;      //y-Koordinate des Bezugspunktes
        double d1;      //Abstand zwischen dem Bezugspunkt und dem ersten Punkt
        int anzahl = poi.length;    //anzahl wird auf Laenge des eingegebenen Arrays gesetzt
        int speicher = 0;       //Initialisierung des Speichers zur Speicherung des bisher kuerzeseten Abstandes
        int unten = 1;      //Variable zum Tauschen der Array-Variablen

        for(int bezugspunkt = 0; bezugspunkt < anzahl - 1; bezugspunkt++)       //for-Methode zur Extraktion der x, y -Werte
        {
            double kuerzester_abstand = 1500;   // preset,weil groeßer als maximaler Abstand bei 1000x1000 Spielfeld 
            x = poi[bezugspunkt].getX();
            y = poi[bezugspunkt].getY();
            for(int abstand = bezugspunkt + 1; abstand < anzahl; abstand++)     //for-Methode zum Bestimmen der Abstände
            {
                d1 = poi[abstand].gibAbstand(x, y);
                if(d1 < kuerzester_abstand)     //Vergleich vom kuerzsten Abstand mit dem zu pruefenden Abstand
                {
                    kuerzester_abstand = d1;    //falls d1 kuerzer ist als der bisher kuerzeste Abstand wird d1 zum kuerzesten Abstand
                    speicher = abstand;         //der Array-Index des jetzt kuerzesten Abstandes wird in "speicher" gemerkt
                }
            }
            System.out.println("Koordinaten des " + unten + ". Punktes: ");
            poi[speicher].ausgabeAttribute();
            tauschen(speicher, unten);  // tauscht den Punkt mit dem kuerzesten Abstand zum Vorpunkt mit dem jetzigen Punkt          
            unten++;    // zum nächsten Index gehen
            System.out.println("Abstand zu diesem Punkt= " + kuerzester_abstand);
        }

        return poi;
    }
    
    // tauscht die Punkte von zwei angegebenen indexen
    private void tauschen(int index1, int index2)       
    {
        Punkt temp = punkte_array[index1];      // speichert den Punkt temporaer
        
        // Tauschen der Punkte
        this.punkte_array[index1] = punkte_array[index2];
        this.punkte_array[index2] = temp;
    }
    
    // berechnet den kuerzesten Weg, um alle POI abzufahren
    public double poiAbfahren()
    {
        double kuerzester_weg = 0;      
        Punkt[] punkte;     // Initialisieren vonm array der Punkte
        punkte = poiSortieren(punkteEingeben());    // punkte setzt sich zusammen aus den sortierten Eingaben von punkteEingeben()
        int anzahl = punkte.length;     // Anzahl der Punkte
        double entfernung = 0;      // Entfernung zw. zwei Punkten
        int x;      // x - Koordinate
        int y;      // y - Koordinate
        
        // addiert alle Entfernungen zw. den Punkten
        for(int bezugspunkt = 0; bezugspunkt < anzahl - 1; bezugspunkt++)
        {
            x = punkte[bezugspunkt].getX();
            y = punkte[bezugspunkt].getY();
            
            // berechnet die Entfernung zw. einem Punkt und seinem Nachfolger
            entfernung = punkte[bezugspunkt + 1].gibAbstand(x,y);
            
            kuerzester_weg += entfernung;

        }
        
        System.out.println("Der kuerzeste Weg ist: " + kuerzester_weg);
        
        return kuerzester_weg;    
    }
    
    // erzeugt Hindernisse auf dem Spielfeld
    public ArrayList<Figur> hindernislisteErzeugen()
    {
        System.out.println("Wie viele Hindernisse sollen sich auf dem Spielfeld befinden?");
        Scanner sc = new Scanner(System.in);    // Initialisieren des Scanners
        int anzahl_hindernisse = sc.nextInt();  // Anzahl der zu erstellenden Hindernisse

        ArrayList<Rechteck> Hindernisse = new ArrayList<Rechteck>();    // erstellt Array fuer Hindernisse
        int ueberlappungen = 0;     // Anzahl an Ueberlappungen insgesamt
        
        // erstellt zufaellige Hindernisse  (Rechtecke)
        for (int i = 0; i < anzahl_hindernisse; i++)
        {
            int k = 0;
            int x = zufallszahl(0,(breite-100));    // x - Koordiante (linke obere Ecke)
            int y = zufallszahl(0,(laenge-100));    // y - Koordinate (linke obere Ecke)
            int laenge = zufallszahl(1,100);         // zufaellige Laenge zw. 1 und 100
            int breite = zufallszahl(1,100);        // zufaellige Breite zw. 1 und 100
            Color farbe = zufallsfarbe();           // weist zufaellige Farbe zu
            String bezeichnung = "Rechteck" + i;    // Rechteck wird Name + index zugewiesen
            Rechteck A = new Rechteck(new Punkt(x,y),laenge,breite,bezeichnung,farbe);       // erstellt Rechteck
            
            // ueberprueft, ob mehr als 50x Rechtecke sich beim Erstellen ueberlappt haben, wenn ja, werden keine weiteren erstellt
            if(ueberlappungen == 50)
            {
                System.out.println("Es konnten nur " + i + " Rechtecke erzeugt werden.");
                break;
            }
            
            // ueberprueft fuer erstelltes Rechteck, ob es andere Rechtecke oder Roboter ueberlappt
            if (i>0)
            {
                int a = 0; // Indexzaehler
                int ueberlappungen_nicht = 0;   // Anzahl Nicht-Ueberlappungen
                while(a < Hindernisse.size())
                {
                    boolean ueberlappt = A.ueberlappt(Hindernisse.get(a)); // boolean, ob erstelltes Rechteck ein anderes ueberlappt
                    boolean ueberlapptRoboter = r.ueberlapptRoboter(Hindernisse.get(a));
                    if(ueberlappt || ueberlapptRoboter)
                    {
                        if(ueberlappt)
                        {
                            ueberlappungen++;// eine Ueberlappung addiert
                        }
                        i--;        // es muss ein neues Rechteck erstellt werden
                        break;
                    }
                    else
                    {
                        ueberlappungen_nicht++;     
                        if(ueberlappungen_nicht == Hindernisse.size())  // wenn es alle bisherigen Hindernisse nicht ueberlappt, wird es zum array hinzugefuegt
                        {
                            Hindernisse.add(A);
                            ueberlappungen = 0;
                            break;
                        }
                        a++;    
                    }
                }
            }
            else
            {
                if (r.ueberlapptRoboter(A))
                {
                    i--;        // es muss ein neues Rechteck erstellt werden
                }  
                else
                {
                    Hindernisse.add(A);     // erstes Rechteck wird nie ein anderes ueberlappen
                }
            }
        }
        
        ArrayList<Figur> Figuren = new ArrayList<Figur>();
        for (int i = 0; i<Hindernisse.size(); i++)
        {
            Figuren.add(new Rechteck(Hindernisse.get(i).getPosition(),
                                Hindernisse.get(i).maxX() - Hindernisse.get(i).minX(),
                                Hindernisse.get(i).maxY() - Hindernisse.get(i).minY(),
                                Hindernisse.get(i).getBezeichnung(), Hindernisse.get(i).getFarbe()));
        }
        
        return Figuren;
    }
    
    // erstellt eine Zufallszahl im gegebenen Intervall
    private int zufallszahl(int von, int bis)
    {
        int zufallszahl = zufallsgenerator.nextInt(bis - von) + von;
        return zufallszahl;
    }
    
    // erstellt eine Zufallsfarbe aus zufaelligen RGB Werten zw. 0 und 255
    private Color zufallsfarbe()
    {
        int R = zufallsgenerator.nextInt(255);
        int G = zufallsgenerator.nextInt(255);
        int B = zufallsgenerator.nextInt(255);
        Color zufallsfarbe = new Color(R, G, B);
        return zufallsfarbe;
    }
    
    // main Methode, ueber welche die anderen ausgefuert werden koennen
    public static void main(String[] args)
    {
        boolean a = true;   // aufrecht erhalten der while loop
        Spielfeld s = new Spielfeld();  // Erstellen eines Spielfeldes

        while (a)
        {
            // Auskunft ueber Wahlmoeglichkeiten
            System.out.println("Was soll ich fuer sie tun?" + "\n" + 
                "\t" + "1 ~ POIs zeichnen und abfahren" + "\n" +
                "\t" + "2 ~ Hindernisse umfahren" + "\n" +
                "\t" + "3 ~ Ihre Fragen beantworten");

            Scanner sc = new Scanner(System.in);    // Initialisierung des Scanners
            String eingabe = sc.nextLine();         // liest Antwort des Ssers
            eingabe = eingabe.toLowerCase();        // macht alles zu Kleinbuchstaben

            if (eingabe.equals("ende"))break;       // beendet main Methode, falls "ende" engegeben wird

            switch (eingabe)    // je nach Wahl des users wird die jeweilige Methode ausgefuehrt
            {               
                case "1":
                    s.poiZeichnen();
                    break;
                    
                case "2":
                    s.hindernisseUmfahren();
                    break;

                case "3":
                    s.r.spracherkennung();
                    break;
                    
                default:
                    System.out.println("Leider ist das keine meiner Aufgaben - Bitte wähle eine Zahl zwischen 1 und 3!");
            }

        }
    }
    
    // zeichnet eine neue Leinwand mit Hindernissen
    public void zeichnen(ArrayList<Figur> hindernisse, Roboter r)
    {     
        leinwand.zeichnen(hindernisse);
    }
    
    // Roboter umfaehrt erstellte Hindernisse
    public void hindernisseUmfahren()
    {
        ArrayList<Figur> hindernisse = hindernislisteErzeugen();     // initialisieren Hindernisse
        r.position = new Punkt(); // setzt die Position des Roboters auf (0,0) - wichtig bei doppelter Verwendung
        zeichnen(hindernisse,r); // zeichnen der Hindernisse + Roboter in Startposition
        
        // waerend Roboter noch nicht an Zielposition ist
        while (!((breite - r.maxX() == 1) && (laenge - r.maxY() == 1)))
        {
            boolean anEcke = false;     // boolean, ob Roboter genau auf der Ecke eines Rechteckes stoeßt     
            boolean hindernisimweg_x = false;   // boolean, ob Hindernis im Weg in x-Richtung
            boolean hindernisimweg_y = false;   // boolean, ob Hindernis im Weg in y-Richtung
            
            // ueberprueft fuer jedes Hindernis, ob und wie es im Weg ist
            for (int hinderniss_nummer = 0;hinderniss_nummer<hindernisse.size();hinderniss_nummer++)
            {   
                if ((r.zwischenX(hindernisse.get(hinderniss_nummer))))
                {   
                    hindernisimweg_y = true;
                }
                if ((r.zwischenY(hindernisse.get(hinderniss_nummer))))
                {
                    hindernisimweg_x = true;
                }
                // ueberprueft ob von einem Rechteck beide im Weg sind == trifft auf Ecke
                if (r.zwischenX(hindernisse.get(hinderniss_nummer)) && r.zwischenY(hindernisse.get(hinderniss_nummer)))
                    anEcke = true;
            }
            
            // falls nichts im Weg nach rechts ist und der Roboter noch nicht an der rechten Wand ist
            if (!hindernisimweg_x && !r.anWandX(breite) )
            {
                r.bewegeUm(new Punkt(1,0));     // ein Pixel nach rechts bewegen
            }
            
            // falls nichts im Weg nach unten ist und der Roboter noch nicht an der unteren Wand ist
            if ((!hindernisimweg_y || anEcke) && !r.anWandY(laenge))
            {
                r.bewegeUm(new Punkt(0,1));     // einen Pixel nach unten bewegen
            }
            
            // breaked aus der loop, falls der Roboter fest steckt
            if ((!anEcke && hindernisimweg_x && hindernisimweg_y) || (r.anWandX(breite) && hindernisimweg_y) || 
            (r.anWandY(laenge) && hindernisimweg_x) )break;
            
            leinwand.warten(16);
            zeichnen(hindernisse,r);    // zeichnet Hindernisse und nue Position des Roboters
        }
    }
    
    // Roboter faehrt grafisch alle poi ab
    public void poiZeichnen()
    {
        ArrayList<Figur> poi = new ArrayList<Figur>();     // Initialisieren vonm array der Punkte
        poi = poiUmwandeln(poiSortieren(punkteEingeben()));    // punkte setzt sich zusammen aus den sortierten Eingaben von punkteEingeben()
        Punkt abstand;  // Verschiebevektor zum naechsten Punkt
        
        r.position = new Punkt(); // setzt die Position des Roboters auf (0,0) - wichtig bei doppelter Verwendung
        zeichnen(poi, r); // zeichnen poi und Roboter in Startposition
        // int norm = 0;
        // Punkt norm_abstand;
        
        // geht jeden Punkt durch
        for(int aktueller_punkt = 0; aktueller_punkt < poi.size() - 1; aktueller_punkt++)
        {
            // berechnet den Vektor, um welchen der Roboter insgesamt verschoben werden muss, um den naechsten Punkt zu erreichen
            abstand = new Punkt((poi.get(aktueller_punkt + 1).minX() - poi.get(aktueller_punkt).minX()),
                                (poi.get(aktueller_punkt + 1).minY() - poi.get(aktueller_punkt).minY()));
            
            /*  
             * Versuch das ganze linear zu gestalten (wird nicht genutzt)
            norm = (int) (Math.round(r.getPosition().gibAbstand(poi.get(aktueller_punkt + 1).minX(),poi.get(aktueller_punkt + 1).minY())/
                    (0.5*(abstand.getX()+abstand.getY()))));                               
                                
            norm_abstand = new Punkt(Math.round(abstand.getX()/norm), Math.round(abstand.getY()/norm));
            
            for (int i = 0; i < norm; i++)
            {
                r.bewegeUm(norm_abstand);
                leinwand.warten(16);
                zeichnen(poi, r);
            }
            */
            
            // fahren, bis Roboter x-Wert erreicht hat
            while(!(r.getPosition().getX() == poi.get(aktueller_punkt + 1).minX()))
            {
                if (r.getPosition().getX() < poi.get(aktueller_punkt + 1).minX())
                {
                    r.bewegeUm(new Punkt(1,0));     // falls der Punkt rechts liegt, bewegt er sich nach rechts
                }
                else
                {
                    r.bewegeUm(new Punkt(-1,0));    // falls der Punkt links liegt, bewegt er sich nach links
                }
                
                leinwand.warten(16);
                zeichnen(poi, r);
            }
            
            // fahren bis Roboter y-Wert erreicht hat
            while(!(r.getPosition().getY() == poi.get(aktueller_punkt + 1).minY()))
            {
                if (r.getPosition().getY() < poi.get(aktueller_punkt + 1).minY())
                {
                    r.bewegeUm(new Punkt(0,1));     // falls der Punkt unterhalb liegt, bewegt er sich nach unten
                }
                else
                {
                    r.bewegeUm(new Punkt(0,-1));    // falls der Punkt oberhalb liegt, bewegt er sich nach oben
                }
                
                leinwand.warten(16);
                zeichnen(poi, r);
            }
        
        }
    }
    
    // wandelt unser sortiertes Punkte Array in ein ArrayList<Figur> mit Objekten von POI um
        public ArrayList<Figur> poiUmwandeln(Punkt[] poi)
    {
        Punkt[] punkte = poi;
        ArrayList <Figur> POIs = new ArrayList<Figur>();
        for (int i=0; i < punkte.length; i++)
        {
            POIs.add(new POI(punkte[i]));        
        }
        return POIs;
    }
}
