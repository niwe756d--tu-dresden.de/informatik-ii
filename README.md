# Informatik II Projekt

**von Alex Blandón und Niklas Weber im Rahmen der Informatik 2 Vorlesung**


**Das Programm wird hier ausgeführt:**

```bash
Java\package.bluej
```

**Der Roboter kann folgendes:**

**Welche dieser Aufgaben Sie ausführen wollen, steuern Sie über die ``main`` Methode**
**in der Klasse ``Spielfeld``.**

```java
poiZeichnen()
/* 
Der Roboter fährt durch den User angegebene Punkte grafisch ab und gibt den kürzesten Weg an.
Hierbei werden zuerst die jeweiligen Punktkoordinaten vom User abgefragt und danach sortiert,
sodass von einem Punkt zum nächsten jeweils der kurztmöglichste Abstand herrscht, dieser wird 
wie der Gesamtweg über die Konsole ausgegeben.
*/

hindernisseUmfahren()
/* 
Es wird ein Spielfeld mit zufälligen Hindernissen erstellt, welche vom Roboter umfahren werden.
Anfangs wird der User gefragt, wie viele Hindernisse er erstellen will. Abhängig von dieser
Angabe werden nun zufällige Rechtecke (in Länge, Breite, Farbe) auf dem Spielfeld erzeugt,
solange es bei der Erstellung nicht zu zu vielen (50) Überlappungen von Rechtecken kommt.
Anschließend umfährt der Roboter, startend an der linken oberen Spielfeldecke, die Hindernisse,
bis er entweder die rechte untere Ecke erreicht oder zwischen den Hindernissen stecken bleibt.
*/

spracherkennung()
/*
Der Roboter beantworteten Fragen, welche dem User auf der Zunge brennen.
Mit den Stichwörtern "Geschlecht", "Name", "Alter", "Hersteller" kann man
einiges über ihn erfahren.
*/

```


<details><summary><b>Klassendiagramm</b></summary>

![](Klassendiagramm.png)

</details>
